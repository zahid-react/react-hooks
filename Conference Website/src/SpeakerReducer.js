function speakersReducer(state,action){
    function UpdateFavourite(favoriteValue){
      return state.map((item,index) => {
        if (item.id === action.sessionId){
          item.favorite = favoriteValue;
          return item;
        }
        return item;
      })
    }
    switch(action.type){
      case "setSpeakerList":{
        return action.data;
      }
      case "favourite":{
        return UpdateFavourite(true);
      }
      case "unfavourite":{
        return UpdateFavourite(false);
      }
      default:{
        return state;
      }
    }
}

export default speakersReducer;